def sumar(num1, num2):
    resultado: int = num1+num2
    return resultado


def restar(num1, num2):
    resultado: int = num1-num2
    return resultado


print("resultado:", sumar(1, 2))
print("resultado:", sumar(3, 4))
print("resultado:", restar(6, 5))
print("resultado:", restar(8, 7))
